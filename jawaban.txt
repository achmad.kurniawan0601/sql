1. create database myshop;

2. create table categories(
    -> id int(4) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );

   
create table users(
    -> id int(6) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key (id)
    -> );

create table items(
    -> id int(6) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(6),
    -> stock int(6),
    -> category_id int(6),
    -> primary key (id),
    -> foreign key (id) references categories(id)
    -> );

alter table items add categories_id int;
alter table items add Foreign key (categories_id) references items(id);

3.  insert into categories(name) values ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

    insert into users(name, email, password) values(
    -> "John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123")
    -> ;

    insert into items(name, description, price, stock, category_id) value(
    -> "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1)
    -> ;

4. a. select name, email from users;
   b.  - select * from items where price > 1000000;
       - select * from items where name LIKE '%watch';
   c. select * from items inner join categories on items.category_id = categories.id;

5. update items set name = 'sumsang', price = 2500000 where id = 1;